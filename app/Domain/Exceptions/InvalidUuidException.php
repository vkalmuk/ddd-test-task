<?php

declare(strict_types=1);

namespace App\Domain\Exceptions;

class InvalidUuidException extends ValidationException
{
    public static function fromString(string $string): static
    {
        return static::create(sprintf("Invalid UUID string: %s", $string));
    }
}
