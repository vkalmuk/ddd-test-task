<?php

declare(strict_types=1);

namespace App\Domain\Exceptions;

class LogicException extends \LogicException
{
    public static function create(string $message = "", int $code = 0, \Throwable | null $previous = null): static
    {
        return new static($message, $code, $previous);
    }
}
