<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

class VOAddress
{
    private function __construct(
        private string $city,
        private string $streetAddress,
        private string $zipCode,
    ) {
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(string $streetAddress): self
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }
    public static function create(string $city, string $street, string $zip): static
    {
        return new static($city, $street, $zip);
    }
}
