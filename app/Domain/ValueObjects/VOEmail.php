<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

use App\Infrastructure\Exceptions\Assert;

class VOEmail extends VOString
{
    protected static function assert(string $value): void
    {
        Assert::email($value, 'Email address should be valid');
    }
}
