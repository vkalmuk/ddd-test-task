<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

class VOInteger
{
    private function __construct(
        private readonly int $value,
    ) {
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public static function fromInt(int $value): static
    {
        static::assertAmount($value);

        return new static($value);
    }

    protected static function assertAmount(int $value): void
    {
    }
}
