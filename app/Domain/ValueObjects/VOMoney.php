<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

use App\Domain\Enums\CurrencyEnum;
use Webmozart\Assert\Assert;

class VOMoney
{
    private function __construct(
        protected readonly int $amount,
        protected readonly CurrencyEnum $currency,
    ) {
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrency(): CurrencyEnum
    {
        return $this->currency;
    }

    public static function create(int $amount, CurrencyEnum $currency = CurrencyEnum::USD): static
    {
        static::assertAmount($amount);

        return new static($amount, $currency);
    }

    protected static function assertAmount(int $value): void
    {
    }
}
