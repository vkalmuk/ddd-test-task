<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

class VOPhone extends VOString
{
    protected static function assert(string $value): void
    {
        //We can add phone number validation
    }
}
