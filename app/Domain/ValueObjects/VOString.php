<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

class VOString implements \Stringable
{
    private function __construct(
        protected readonly string $value,
    ) {
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public static function fromString(string $value): static
    {
        static::assertString($value);

        return new static($value);
    }

    public static function assertString(string $value): void
    {
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
