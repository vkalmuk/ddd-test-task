<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

use App\Domain\Exceptions\InvalidUuidException;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class VOUuidId implements \Stringable
{
    private function __construct(
        protected readonly UuidInterface $value,
    ) {
    }

    public function getValue(): UuidInterface
    {
        return $this->value;
    }

    public static function fromUuid(UuidInterface $uuid): static
    {
        return new static($uuid);
    }

    public static function fromString(string $id): static
    {
        try {
            $uuid =  new static(Uuid::fromString($id));
        } catch (InvalidUuidStringException) {
            throw InvalidUuidException::fromString($id);
        }

        return $uuid;
    }

    public function __toString(): string
    {
        return $this->value->toString();
    }
}
