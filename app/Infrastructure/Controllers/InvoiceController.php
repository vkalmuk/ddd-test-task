<?php

declare(strict_types=1);

namespace App\Infrastructure\Controllers;

use App\Infrastructure\Controller;
use App\Modules\Invoices\Api\InvoiceMapperInterface;
use App\Modules\Invoices\Contracts\InvoiceManagerInterface;
use App\Modules\Invoices\Contracts\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Mapper\InvoiceApprovalMapper;
use App\Modules\Invoices\ValueObjects\InvoiceId;
use Illuminate\Http\JsonResponse;

class InvoiceController extends Controller
{
    public function __construct(
        private readonly InvoiceManagerInterface $invoiceManager,
        private readonly InvoiceRepositoryInterface $invoiceRepository,
        private readonly InvoiceMapperInterface $invoiceMapper,
        private readonly InvoiceApprovalMapper $invoiceApprovalMapper,
    ) {
    }

    public function show(string $id): JsonResponse
    {
        $invoiceId = InvoiceId::fromString($id);
        $invoice = $this->invoiceRepository->getByInvoiceId($invoiceId);

        return JsonResponse::fromJsonString(json_encode($this->invoiceMapper->mapOutputDto($invoice)));
    }

    public function approve(string $id): JsonResponse
    {
        $invoiceId = InvoiceId::fromString($id);
        $invoice = $this->invoiceRepository->getByInvoiceId($invoiceId);
        $approveDto = $this->invoiceApprovalMapper->mapApprovalDto($invoice);
        $this->invoiceManager->approve($approveDto);

        return new JsonResponse();
    }

    public function reject(string $id): JsonResponse
    {
        $invoiceId = InvoiceId::fromString($id);
        $invoice = $this->invoiceRepository->getByInvoiceId($invoiceId);
        $approveDto = $this->invoiceApprovalMapper->mapApprovalDto($invoice);
        $this->invoiceManager->reject($approveDto);

        return new JsonResponse();
    }
}
