<?php

declare(strict_types=1);

namespace App\Infrastructure\Exceptions;

use App\Domain\Exceptions\ValidationException;
use Webmozart\Assert\Assert as WebmozartAssert;

class Assert extends WebmozartAssert
{
    protected static function reportInvalidArgument(mixed $message): void
    {
        throw ValidationException::create((string)$message);
    }
}
