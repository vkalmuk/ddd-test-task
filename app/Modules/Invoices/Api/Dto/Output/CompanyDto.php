<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto\Output;

class CompanyDto
{
    public function __construct(
        public readonly string $name,
        public readonly string $streetAddress,
        public readonly string $city,
        public readonly string $zipCode,
        public readonly string $email,
        public readonly string $phone,
    ) {
    }
}
