<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto\Output;

use App\Domain\Enums\CurrencyEnum;

class InvoiceDto
{
    public function __construct(
        public readonly string $invoiceNumber,
        public readonly string $invoiceDate,
        public readonly string $dueDate,
        public readonly float $totalPrice,
        public readonly CompanyDto $company,
        public readonly CompanyDto $billedCompany,
        public readonly array $products,
        public readonly string $currency = CurrencyEnum::USD->value,
    ) {
    }
}
