<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto\Output;

class ProductDto
{
    public function __construct(
        public readonly string $name,
        public readonly int $quantity,
        public readonly float $unitPrice,
        public readonly float $total,
        public readonly string $currency,
    ) {
    }
}
