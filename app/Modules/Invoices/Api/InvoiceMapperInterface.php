<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api;

use App\Modules\Invoices\Api\Dto\Output\InvoiceDto;
use App\Modules\Invoices\Model\Entities\Invoice;

interface InvoiceMapperInterface
{
    public function mapOutputDto(Invoice $invoice): InvoiceDto;
}
