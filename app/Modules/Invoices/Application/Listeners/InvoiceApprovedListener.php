<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Listeners;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Invoices\Contracts\InvoiceManagerInterface;
use App\Modules\Invoices\Contracts\InvoiceRepositoryInterface;
use App\Modules\Invoices\Model\Entities\Invoice;
use App\Modules\Invoices\ValueObjects\InvoiceId;
use App\Modules\Invoices\ValueObjects\InvoiceStatus;

class InvoiceApprovedListener
{
    public function __construct(
        private readonly InvoiceRepositoryInterface $invoiceRepository,
        private readonly InvoiceManagerInterface $invoiceManager,
    ) {
    }

    public function handle(EntityApproved $event): void
    {
        if (Invoice::class === $event->approvalDto->entity) {
            $invoice = $this->invoiceRepository->getByInvoiceId(InvoiceId::fromUuid($event->approvalDto->id));
            $invoice->setStatus(InvoiceStatus::fromEnum(StatusEnum::APPROVED));
            $this->invoiceManager->save($invoice);
        }
    }
}
