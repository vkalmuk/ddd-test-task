<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Contracts;

use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Model\Entities\Invoice;

interface InvoiceManagerInterface
{
    public function approve(ApprovalDto $approveDto): void;

    public function reject(ApprovalDto $approveDto): void;

    public function save(Invoice $invoice): void;
}
