<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Contracts;

use App\Modules\Invoices\Model\Entities\Invoice;
use App\Modules\Invoices\ValueObjects\InvoiceId;

interface InvoiceRepositoryInterface
{
    public function getByInvoiceId(InvoiceId $invoiceId): Invoice;
}
