<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Exceptions;

use App\Domain\Exceptions\LogicException;

class InvalidInvoiceStatus extends LogicException
{
    public static function fromString(string $string): static
    {
        return static::create(sprintf("Invalid InvoiceStatus string: %s", $string));
    }
}
