<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Factories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Infrastructure\Database\Models\Company;
use App\Modules\Invoices\Infrastructure\Database\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

class InvoiceFactory extends Factory
{
    protected $model = Invoice::class;
    public function definition(): array
    {
        $date = Carbon::today()->subDays(rand(0, 365));
        $faker = \Faker\Factory::create();

        return [
            'id' => Uuid::uuid4()->toString(),
            'number' => $faker->uuid(),
            'date' => $date->format('Y-m-d'),
            'due_date' => $date->addDays(rand(1, 40))->format('Y-m-d'),
            'status' => StatusEnum::cases()[array_rand(StatusEnum::cases())],
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
