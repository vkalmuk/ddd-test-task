<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database;

use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Contracts\InvoiceManagerInterface;
use App\Modules\Invoices\Infrastructure\Database\Models\Invoice as EloquentInvoice;
use App\Modules\Invoices\Model\Entities\Invoice;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InvoiceManager implements InvoiceManagerInterface
{
    public function __construct(
        private readonly ApprovalFacadeInterface $approvalFacade,
    ) {
    }

    public function approve(ApprovalDto $approveDto): void
    {
        $this->approvalFacade->approve($approveDto);
    }

    public function reject(ApprovalDto $approveDto): void
    {
        $this->approvalFacade->reject($approveDto);
    }

    public function save(Invoice $invoice): void
    {
        $invoiceModel = EloquentInvoice::find($invoice->getId()->getValue());
        if (null === $invoiceModel) {
            throw new ModelNotFoundException();
        }
        $invoiceModel->status = $invoice->getStatus()->getValue()->value;
        $invoiceModel->save();
    }
}
