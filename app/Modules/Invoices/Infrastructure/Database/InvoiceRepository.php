<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database;

use App\Modules\Invoices\Contracts\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Database\Models\Company as EloquentCompany;
use App\Modules\Invoices\Infrastructure\Database\Models\Invoice as EloquentInvoice;
use App\Modules\Invoices\Model\Entities\Company;
use App\Modules\Invoices\Model\Entities\Invoice;
use App\Modules\Invoices\Model\Entities\InvoiceLine;
use App\Modules\Invoices\Model\Entities\Product;
use App\Modules\Invoices\ValueObjects\CompanyAddress;
use App\Modules\Invoices\ValueObjects\CompanyEmail;
use App\Modules\Invoices\ValueObjects\CompanyId;
use App\Modules\Invoices\ValueObjects\CompanyName;
use App\Modules\Invoices\ValueObjects\CompanyPhone;
use App\Modules\Invoices\ValueObjects\InvoiceDates;
use App\Modules\Invoices\ValueObjects\InvoiceId;
use App\Modules\Invoices\ValueObjects\InvoiceLineId;
use App\Modules\Invoices\ValueObjects\InvoiceLineQuantity;
use App\Modules\Invoices\ValueObjects\InvoiceNumber;
use App\Modules\Invoices\ValueObjects\InvoiceStatus;
use App\Modules\Invoices\ValueObjects\ProductId;
use App\Modules\Invoices\ValueObjects\ProductName;
use App\Modules\Invoices\ValueObjects\ProductPrice;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Ramsey\Collection\Collection;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    public function getByInvoiceId(InvoiceId $invoiceId): Invoice
    {
        $invoice = EloquentInvoice::find($invoiceId->getValue());
        if (null === $invoice) {
            throw new ModelNotFoundException();
        }
        $productLinesArray = $this->mapProducts($invoice->products);
        $productLines = new Collection(InvoiceLine::class, $productLinesArray);

        return new Invoice(
            InvoiceId::fromString($invoice->id),
            InvoiceStatus::fromString($invoice->status),
            InvoiceNumber::fromString($invoice->number),
            InvoiceDates::fromStrings($invoice->date, $invoice->due_date),
            $this->mapCompany($invoice->company),
            $this->mapCompany($invoice->billedCompany),
            $productLines,
        );
    }
    private function mapCompany(EloquentCompany $company): Company
    {
        return new Company(
            CompanyId::fromString($company->id),
            CompanyName::fromString($company->name),
            CompanyAddress::create($company->city, $company->street, $company->zip),
            CompanyPhone::fromString($company->phone),
            CompanyEmail::fromString($company->email),
        );
    }

    private function mapProducts(EloquentCollection $products): array
    {
        $result = [];
        foreach ($products as $product) {
            $result[] = new InvoiceLine(
                InvoiceLineId::fromString($product->pivot->id),
                InvoiceLineQuantity::fromInt($product->pivot->quantity),
                new Product(
                    ProductId::fromString($product->id),
                    ProductName::fromString($product->name),
                    ProductPrice::create($product->price),
                ),
            );
        }

        return $result;
    }
}
