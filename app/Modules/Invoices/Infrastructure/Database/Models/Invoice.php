<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Models;

use App\Modules\Invoices\Infrastructure\Database\Factories\InvoiceFactory;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Invoice extends Model
{
    use HasUuids;
    use HasFactory;


    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function billedCompany(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'billed_company_id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'invoice_product_lines')->withPivot(['id', 'quantity']);
    }

    protected static function newFactory(): Factory
    {
        return InvoiceFactory::new();
    }
}
