<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Mapper;

use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Model\Entities\Invoice;

class InvoiceApprovalMapper
{
    public function mapApprovalDto(Invoice $invoice): ApprovalDto
    {
        return new ApprovalDto($invoice->getId()->getValue(), $invoice->getStatus()->getValue(), Invoice::class);
    }
}
