<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Mapper;

use App\Modules\Invoices\Api\Dto\Output\CompanyDto;
use App\Modules\Invoices\Api\Dto\Output\InvoiceDto;
use App\Modules\Invoices\Api\Dto\Output\ProductDto;
use App\Modules\Invoices\Api\InvoiceMapperInterface;
use App\Modules\Invoices\Model\Entities\Company;
use App\Modules\Invoices\Model\Entities\Invoice;
use Ramsey\Collection\Collection;

class InvoiceMapper implements InvoiceMapperInterface
{
    public function mapOutputDto(Invoice $invoice): InvoiceDto
    {
        $products = $this->mapProductDtoArray($invoice->getLineItems());
        $totalPrice = 0;
        foreach ($invoice->getLineItems() as $lineItem) {
            $totalPrice += $lineItem->getQuantity()->getValue() * $lineItem->getProduct()->getPrice()->getAmount() / 100;
        }

        return new InvoiceDto(
            $invoice->getNumber()->getValue(),
            $invoice->getDates()->getInvoiceDate()->format('d/m/Y'),
            $invoice->getDates()->getDueDate()->format('d/m/Y'),
            $totalPrice,
            $this->mapCompanyDto($invoice->getCompany()),
            $this->mapCompanyDto($invoice->getBilledCompany()),
            $products,
        );
    }

    private function mapCompanyDto(Company $company): CompanyDto
    {
        return new CompanyDto(
            $company->getName()->getValue(),
            $company->getAddress()->getStreetAddress(),
            $company->getAddress()->getCity(),
            $company->getAddress()->getZipCode(),
            $company->getEmail()->getValue(),
            $company->getPhone()->getValue(),
        );
    }

    private function mapProductDtoArray(Collection $lineItems): array
    {
        $result = [];
        foreach ($lineItems as $lineItem) {
            $result [] = new ProductDto(
                $lineItem->getProduct()->getName()->getValue(),
                $lineItem->getQuantity()->getValue(),
                $lineItem->getProduct()->getPrice()->getAmount() / 100,
                $lineItem->getQuantity()->getValue() * $lineItem->getProduct()->getPrice()->getAmount() / 100,
                $lineItem->getProduct()->getPrice()->getCurrency()->value,
            );
        }

        return $result;
    }
}
