<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Invoices\Api\InvoiceMapperInterface;
use App\Modules\Invoices\Contracts\InvoiceManagerInterface;
use App\Modules\Invoices\Contracts\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Database\InvoiceManager;
use App\Modules\Invoices\Infrastructure\Database\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Mapper\InvoiceMapper;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class InvoiceServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(InvoiceRepositoryInterface::class, InvoiceRepository::class);
        $this->app->scoped(InvoiceMapperInterface::class, InvoiceMapper::class);
        $this->app->scoped(InvoiceManagerInterface::class, InvoiceManager::class);
    }

    /** @return array<class-string> */
    public function provides(): array
    {
        return [
            InvoiceRepositoryInterface::class,
            InvoiceMapperInterface::class,
            InvoiceManagerInterface::class,
        ];
    }
}
