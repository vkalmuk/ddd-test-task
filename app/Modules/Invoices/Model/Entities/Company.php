<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Model\Entities;

use App\Modules\Invoices\ValueObjects\CompanyAddress;
use App\Modules\Invoices\ValueObjects\CompanyEmail;
use App\Modules\Invoices\ValueObjects\CompanyId;
use App\Modules\Invoices\ValueObjects\CompanyName;
use App\Modules\Invoices\ValueObjects\CompanyPhone;

class Company
{
    public function __construct(
        private CompanyId $id,
        private CompanyName $name,
        private CompanyAddress $address,
        private CompanyPhone $phone,
        private CompanyEmail $email,
    ) {
    }

    public function getId(): CompanyId
    {
        return $this->id;
    }

    public function setId(CompanyId $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): CompanyName
    {
        return $this->name;
    }

    public function setName(CompanyName $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): CompanyAddress
    {
        return $this->address;
    }

    public function setAddress(CompanyAddress $number): self
    {
        $this->address = $number;

        return $this;
    }

    public function getPhone(): CompanyPhone
    {
        return $this->phone;
    }

    public function setPhone(CompanyPhone $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): CompanyEmail
    {
        return $this->email;
    }

    public function setEmail(CompanyEmail $email): self
    {
        $this->email = $email;

        return $this;
    }
}
