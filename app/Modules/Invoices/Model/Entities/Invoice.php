<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Model\Entities;

use App\Modules\Invoices\ValueObjects\InvoiceDates;
use App\Modules\Invoices\ValueObjects\InvoiceId;
use App\Modules\Invoices\ValueObjects\InvoiceNumber;
use App\Modules\Invoices\ValueObjects\InvoiceStatus;
use Ramsey\Collection\Collection;

class Invoice
{
    public function __construct(
        private InvoiceId           $id,
        private InvoiceStatus       $status,
        private InvoiceNumber       $number,
        private InvoiceDates        $dates,
        private Company             $company,
        private Company             $billedCompany,
        private readonly Collection $lineItems,
    ) {
    }

    public function getId(): InvoiceId
    {
        return $this->id;
    }

    public function setId(InvoiceId $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStatus(): InvoiceStatus
    {
        return $this->status;
    }

    public function setStatus(InvoiceStatus $status): self
    {
        $this->status = $status;

        return $this;
    }
    public function getNumber(): InvoiceNumber
    {
        return $this->number;
    }

    public function setNumber(InvoiceNumber $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDates(): InvoiceDates
    {
        return $this->dates;
    }

    public function setDates(InvoiceDates $dates): self
    {
        $this->dates = $dates;

        return $this;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getBilledCompany(): Company
    {
        return $this->billedCompany;
    }

    public function setBilledCompany(Company $billedCompany): self
    {
        $this->billedCompany = $billedCompany;

        return $this;
    }

    public function getLineItems(): Collection
    {
        return $this->lineItems;
    }
}
