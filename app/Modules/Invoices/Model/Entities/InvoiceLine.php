<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Model\Entities;

use App\Modules\Invoices\ValueObjects\InvoiceLineId;
use App\Modules\Invoices\ValueObjects\InvoiceLineQuantity;

class InvoiceLine
{
    public function __construct(
        private InvoiceLineId $id,
        private InvoiceLineQuantity $quantity,
        private Product $product,
    ) {
    }

    public function getId(): InvoiceLineId
    {
        return $this->id;
    }

    public function setId(InvoiceLineId $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): InvoiceLineQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(InvoiceLineQuantity $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
