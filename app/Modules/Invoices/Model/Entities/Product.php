<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Model\Entities;

use App\Modules\Invoices\ValueObjects\ProductId;
use App\Modules\Invoices\ValueObjects\ProductName;
use App\Modules\Invoices\ValueObjects\ProductPrice;

class Product
{
    public function __construct(
        private ProductId $id,
        private ProductName $name,
        private ProductPrice $price,
    ) {
    }

    public function getId(): ProductId
    {
        return $this->id;
    }

    public function setId(ProductId $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ProductName
    {
        return $this->name;
    }

    public function setName(ProductName $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ProductPrice
    {
        return $this->price;

        return $this;
    }

    public function setPrice(ProductPrice $price): self
    {
        $this->price = $price;

        return $this;
    }
}
