<?php

declare(strict_types=1);

namespace App\Modules\Invoices\ValueObjects;

use App\Domain\ValueObjects\VOEmail;

class CompanyEmail extends VOEmail
{
}
