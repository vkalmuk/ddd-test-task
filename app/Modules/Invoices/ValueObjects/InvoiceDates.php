<?php

declare(strict_types=1);

namespace App\Modules\Invoices\ValueObjects;

use App\Domain\Exceptions\ValidationException;
use App\Infrastructure\Exceptions\Assert;

class InvoiceDates
{
    private function __construct(
        private readonly \DateTimeInterface $invoiceDate,
        private readonly \DateTimeInterface $dueDate,
    ) {
    }

    public function getInvoiceDate(): \DateTimeInterface
    {
        return $this->invoiceDate;
    }

    public function getDueDate(): \DateTimeInterface
    {
        return $this->dueDate;
    }

    public static function fromStrings($date, $dueDate): static
    {
        try {
            return static::create(new \DateTimeImmutable($date), new \DateTimeImmutable($dueDate));
        } catch (\Exception) {
            throw ValidationException::create('Invalid date');
        }
    }

    public static function create(\DateTimeInterface $invoiceDate, \DateTimeInterface $dueDate): static
    {
        Assert::greaterThanEq($dueDate, $invoiceDate, "Due date should be at least invoice date");

        return new static($invoiceDate, $dueDate);
    }
}
