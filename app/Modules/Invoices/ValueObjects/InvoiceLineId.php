<?php

declare(strict_types=1);

namespace App\Modules\Invoices\ValueObjects;

use App\Domain\ValueObjects\VOUuidId;

class InvoiceLineId extends VOUuidId
{
}
