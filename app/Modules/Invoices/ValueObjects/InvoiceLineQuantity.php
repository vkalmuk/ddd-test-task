<?php

declare(strict_types=1);

namespace App\Modules\Invoices\ValueObjects;

use App\Domain\ValueObjects\VOInteger;
use App\Infrastructure\Exceptions\Assert;

class InvoiceLineQuantity extends VOInteger
{
    protected static function assertAmount(int $value): void
    {
        Assert::greaterThanEq($value, 0, 'Quantity should not be negative');
    }
}
