<?php

declare(strict_types=1);

namespace App\Modules\Invoices\ValueObjects;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Exceptions\InvalidInvoiceStatus;

class InvoiceStatus
{
    private function __construct(
        private StatusEnum $value,
    ) {
    }

    public function getValue(): StatusEnum
    {
        return $this->value;
    }

    public static function fromEnum(StatusEnum $enum): static
    {
        return new static($enum);
    }
    public static function fromString(string $string): static
    {
        if (null === $status = StatusEnum::tryFrom($string)) {
            throw InvalidInvoiceStatus::fromString($string);
        }

        return new static($status);
    }
}
