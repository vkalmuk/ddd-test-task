<?php

declare(strict_types=1);

namespace App\Modules\Invoices\ValueObjects;

use App\Domain\ValueObjects\VOMoney;
use App\Infrastructure\Exceptions\Assert;

class ProductPrice extends VOMoney
{
    protected static function assertAmount(int $value): void
    {
        Assert::greaterThanEq($value, 0, 'Product price should not be negative');
    }
}
