<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('invoices', static function (Blueprint $table): void {
            $table->uuid('billed_company_id');
            $table->foreign('billed_company_id')->references('id')->on('companies');
        });
    }

    public function down(): void
    {
        Schema::table('invoices', static function (Blueprint $table): void {
            $table->dropForeign('invoices_billed_company_id_foreign');
            $table->dropColumn('billed_company_id');
        });
    }
};
