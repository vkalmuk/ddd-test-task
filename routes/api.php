<?php

declare(strict_types=1);

use App\Infrastructure\Controllers\InvoiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/invoices/{id}', [InvoiceController::class, 'show']);
Route::patch('/invoices/{id}/approve', [InvoiceController::class, 'approve']);
Route::patch('/invoices/{id}/reject', [InvoiceController::class, 'reject']);
