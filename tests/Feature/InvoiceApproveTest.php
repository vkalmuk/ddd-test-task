<?php

declare(strict_types=1);

namespace Feature;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Infrastructure\Database\Models\Company;
use App\Modules\Invoices\Infrastructure\Database\Models\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class InvoiceApproveTest extends TestCase
{
    use RefreshDatabase;

    public function test_approve_invoice(): void
    {
        $company = Company::factory()->create();
        $billedCompany = Company::factory()->create();
        $invoice = Invoice::factory()
            ->create([
                'company_id' => $company->id,
                'billed_company_id' => $billedCompany->id,
                'status' => StatusEnum::DRAFT,
            ]);
        $response = $this->patch(sprintf('/api/invoices/%s/approve', $invoice->id));
        $response->assertStatus(Response::HTTP_OK);
        $invoice = DB::table('invoices')->find($invoice->id);
        $this->assertEquals('approved', $invoice->status);
    }

    public function test_show_invoice_not_found(): void
    {
        $company = Company::factory()->create();
        $billedCompany = Company::factory()->create();
        $invoice = Invoice::factory()
            ->create([
                'company_id' => $company->id,
                'billed_company_id' => $billedCompany->id,
                'status' => StatusEnum::REJECTED,
            ]);
        $response = $this->patch(sprintf('/api/invoices/%s/approve', $invoice->id));
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $invoice = DB::table('invoices')->find($invoice->id);
        $this->assertEquals('rejected', $invoice->status);
    }
}
