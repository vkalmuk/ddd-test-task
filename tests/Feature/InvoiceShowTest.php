<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Modules\Invoices\Infrastructure\Database\Models\Company;
use App\Modules\Invoices\Infrastructure\Database\Models\Invoice;
use App\Modules\Invoices\Infrastructure\Database\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class InvoiceShowTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_invoice(): void
    {
        $company = Company::factory()->create();
        $billedCompany = Company::factory()->create();
        $invoice = Invoice::factory()
            ->create([
                'company_id' => $company->id,
                'billed_company_id' => $billedCompany->id,
            ]);

        $products = Product::factory()
            ->createMany([
                ['price' => 111,],
                ['price' => 234,],
                ['price' => 710,],
            ]);
        $quantities = [5,3,6];
        foreach ($products as $key => $product) {
            $lines =  [
                'id' => Uuid::uuid4()->toString(),
                'invoice_id' => $invoice->id,
                'product_id' => $product->id,
                'quantity' => $quantities[$key],
                'created_at' => now(),
                'updated_at' => now(),
            ];
            DB::table('invoice_product_lines')->insert($lines);
        }
        $response = $this->get(sprintf('/api/invoices/%s', $invoice->id));

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([
                'invoiceNumber' => $invoice->number,
                'invoiceDate' => (new \DateTimeImmutable($invoice->date))->format('d/m/Y'),
                'dueDate' => (new \DateTimeImmutable($invoice->due_date))->format('d/m/Y'),
                'totalPrice' => (111 * 5 + 234 * 3 + 710 * 6) / 100,
                'currency' => 'usd',
                'company' => [
                    'name' => $company->name,
                    'streetAddress' => $company->street,
                    'city' => $company->city,
                    'zipCode' => $company->zip,
                    'phone' => $company->phone,
                    'email' => $company->email,
                ],
                'billedCompany' => [
                    'name' => $billedCompany->name,
                    'streetAddress' => $billedCompany->street,
                    'city' => $billedCompany->city,
                    'zipCode' => $billedCompany->zip,
                    'phone' => $billedCompany->phone,
                    'email' => $billedCompany->email,
                ],
                'products' => [
                    [
                        'name' => $products[0]->name,
                        'currency' => 'usd',
                        'quantity' => $quantities[0],
                        'unitPrice' => $products[0]->price / 100,
                        'total' => $products[0]->price * $quantities[0] / 100,
                    ],
                    [
                        'name' => $products[1]->name,
                        'currency' => 'usd',
                        'quantity' => $quantities[1],
                        'unitPrice' => $products[1]->price / 100,
                        'total' => $products[1]->price * $quantities[1] / 100,
                    ],
                    [
                        'name' => $products[2]->name,
                        'currency' => 'usd',
                        'quantity' => $quantities[2],
                        'unitPrice' => $products[2]->price / 100,
                        'total' => $products[2]->price * $quantities[2] / 100,
                    ],
                ],
            ])
        ;
    }

    public function test_show_invoice_not_found(): void
    {
        $response = $this->get('/api/invoices/00000000-0000-0000-0000-000000000000');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
