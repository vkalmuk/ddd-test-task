<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\ValueObjects;

use App\Domain\Exceptions\InvalidUuidException;
use App\Domain\Exceptions\ValidationException;
use App\Domain\ValueObjects\VOUuidId;
use App\Modules\Invoices\ValueObjects\InvoiceDates;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class InvoiceDatesTest extends TestCase
{
    public function testCreateInvoiceDates(): void
    {
        $invoiceDate = new \DateTimeImmutable('2024-11-26 10:00:00');
        $dueDate = new \DateTimeImmutable('2024-11-27 10:00:00');
        $invoiceDates = InvoiceDates::create($invoiceDate, $dueDate);
        Assert::assertEquals($invoiceDate, $invoiceDates->getInvoiceDate());
        Assert::assertEquals($dueDate, $invoiceDates->getDueDate());
    }

    public function testGetValue(): void
    {
        $invoiceDate = new \DateTimeImmutable('2024-11-26 10:00:00');
        $dueDate = new \DateTimeImmutable('2024-11-26 09:59:59');
        $this->expectException(ValidationException::class);
        InvoiceDates::create($invoiceDate, $dueDate);
    }
}
