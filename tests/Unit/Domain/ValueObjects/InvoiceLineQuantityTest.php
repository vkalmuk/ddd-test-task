<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\ValueObjects;

use App\Domain\Enums\CurrencyEnum;
use App\Domain\Exceptions\ValidationException;
use App\Modules\Invoices\ValueObjects\InvoiceLineQuantity;
use App\Modules\Invoices\ValueObjects\ProductPrice;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class InvoiceLineQuantityTest extends TestCase
{
    public function testInvoiceLineQuantity(): void
    {
        $money = InvoiceLineQuantity::fromInt(10);
        Assert::assertEquals(10, $money->getValue());
    }

    public function testZeroInvoiceLineQuantity(): void
    {
        $money = InvoiceLineQuantity::fromInt(0);
        Assert::assertEquals(0, $money->getValue());
    }

    public function testNegativeProductPrice(): void
    {
        $this->expectException(ValidationException::class);
        InvoiceLineQuantity::fromInt(-1);
    }
}
