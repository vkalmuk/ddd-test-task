<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\ValueObjects;

use App\Domain\Enums\CurrencyEnum;
use App\Domain\Exceptions\ValidationException;
use App\Domain\ValueObjects\VOMoney;
use App\Modules\Invoices\ValueObjects\ProductPrice;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ProductPriceTest extends TestCase
{
    public function testProductPrice(): void
    {
        $money = ProductPrice::create(10, CurrencyEnum::USD);
        Assert::assertEquals(10, $money->getAmount());
        Assert::assertEquals(CurrencyEnum::USD, $money->getCurrency());
    }

    public function testZeroProductPrice(): void
    {
        $money = ProductPrice::create(0);
        Assert::assertEquals(0, $money->getAmount());
        Assert::assertEquals(CurrencyEnum::USD, $money->getCurrency());
    }

    public function testNegativeProductPrice(): void
    {
        $this->expectException(ValidationException::class);
        ProductPrice::create(-1);
    }
}
