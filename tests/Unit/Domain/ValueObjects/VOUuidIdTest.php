<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\ValueObjects;

use App\Domain\Exceptions\InvalidUuidException;
use App\Domain\ValueObjects\VOUuidId;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class VOUuidIdTest extends TestCase
{
    public function testCreateUuidIdFromValidString(): void
    {
        $uuidId = VOUuidId::fromString('efa02eba-5746-4861-8a1d-6524da7bd712');
        Assert::assertEquals('efa02eba-5746-4861-8a1d-6524da7bd712', $uuidId);
    }

    public function testInvalidUuid(): void
    {
        $this->expectException(InvalidUuidException::class);
        VOUuidId::fromString('not-valid-uuid-12344233');
    }
}
